# Welcome to the Solar Analytics Git Workshop

## Basics

Retrieve the repository and store a copy on your local machine. In the terminal where you want the new directory to go enter:
> git clone https://reybolt@bitbucket.org/reybolt/solar-analytics-git-workshop.git


Now you have a copy of the repository.
If you try typing:
> git status


Assuming you are not working within an existing git repo, this should return an error.
> fatal: Not a git repository (or any of the parent directories): .git

Move to the directory where the repository is stored:
> cd solar-analytics-git-workshop.git


Now try to run this command again:
> git status

You should see the following response:
> On branch master
Your branch is up-to-date with 'origin/master'.
nothing to commit, working directory clean

Let's **add** a text file using your first name as the filename.

> vi reynaldo.txt

Add some words to the text

Try running:
> git status

Then let's add it to staging.
> git add reynaldo.txt

> git status

Let's now commit this change
> git commit -m "Adding a useless file named after me :)"

Check it out as usual:
> git status

Lastly, let's push the commit to the remote repository:
> git push

## Branching


